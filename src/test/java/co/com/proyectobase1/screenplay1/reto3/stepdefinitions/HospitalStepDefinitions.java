package co.com.proyectobase1.screenplay1.reto3.stepdefinitions;

import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase1.screenplay1.reto3.questions.LaRespuesta;
import co.com.proyectobase1.screenplay1.reto3.tasks.AbrirWebHospital;
import co.com.proyectobase1.screenplay1.reto3.tasks.AbrirWebHospitalCita;
import co.com.proyectobase1.screenplay1.reto3.tasks.AbrirWebHospitalPaciente;
import co.com.proyectobase1.screenplay1.reto3.tasks.AgendarPaciente;
import co.com.proyectobase1.screenplay1.reto3.tasks.RegistrarDoctor;
import co.com.proyectobase1.screenplay1.reto3.tasks.RegistrarPaciente;
import cucumber.api.DataTable;

public class HospitalStepDefinitions {
	
private String primerNombre;
	
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicial()
	{
		//le damos la habilidad de navegar en la web a el actor Berto
		carlos.can(BrowseTheWeb.with(hisBrowser)); 
		
	}
	
	@Dado("^que Carlos necesita registrar un nuevo doctor$")
	public void queCarlosNecesitaRegistrarUnNuevoDoctor(){
		carlos.wasAbleTo(AbrirWebHospital.LaPaginaWebHospital());
	    
	}

	@Cuando("^el realiza el ingreso del mismo en el aplicativo de Administracion de Hospitales$")
	public void elRealizaElIngresoDelMismoEnElAplicativoDeAdministracionDeHospitales(DataTable dtDatosForm) throws Exception {
		List<List<String>> data = dtDatosForm.raw();
		String nombreCompleto = "";
		for(int i=0; i<data.size(); i++) {
			carlos.attemptsTo(RegistrarDoctor.DoctorEnFormulario(data, i));
		}
			//primerNombre = data.get(i).get(0).trim();
			//nombreCompleto = data.get(i).get(0).trim()+" "+data.get(i).get(1).trim()+" "+data.get(i).get(2).trim();
			
	    
	}

	@Entonces("^el verifica que se presente en pantalla el mensaje (.*)$")
	public void elVerificaQueSePresenteEnPantallaElMensajeDatosGuardadosCorrectamente(String mensajeEsperado) {
		
	    carlos.should(GivenWhenThen.seeThat(LaRespuesta.es(), Matchers.equalTo(mensajeEsperado)));
	}
	
	@Dado("^que Carlos necesita registrar un nuevo paciente$")
	public void queCarlosNecesitaRegistrarUnNuevoPaciente() throws Exception {
		carlos.wasAbleTo(AbrirWebHospitalPaciente.LaPaginaWebHospitalPaciente());
	    
	}
	@Cuando("^el realiza el ingreso del mismo en el aplicativo de Administracion de Hospitales agregar paciente$")
	public void elRealizaElIngresoDelMismoEnElAplicativoDeAdministracionDeHospitalesAgregarPaciente(DataTable dtDatosForm) throws Exception {
		List<List<String>> data = dtDatosForm.raw();
		
		for(int i=0; i<data.size(); i++) {
			carlos.attemptsTo(RegistrarPaciente.PacienteEnFormulario(data, i));
		}
			
	}
	@Dado("^que Carlos necesita asistir al medico$")
	public void queCarlosNecesitaAsistirAlMedico() throws Exception {
		carlos.wasAbleTo(AbrirWebHospitalCita.LaPaginaWebHospitalCita());
	}

	@Cuando("^el realiza el agendamiento de una Cita$")
	public void elRealizaElAgendamientoDeUnaCita(DataTable dtDatosForm) throws Exception {
           List<List<String>> data = dtDatosForm.raw();
		
		for(int i=0; i<data.size(); i++) {
			carlos.attemptsTo(AgendarPaciente.PacienteEnCita(data, i));
		}
	
	}
	    
}
