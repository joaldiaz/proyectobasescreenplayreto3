package co.com.proyectobase1.screenplay1.reto3.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import java.io.IOException;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features="src/test/resources/features/Hospital.feature",
		
		glue="co.com.proyectobase1.screenplay1.reto3.stepdefinitions",
		snippets=SnippetType.CAMELCASE)
public class RunnerTagsHospital {

}
