#Author: jadiaz@choucairtesting.com
# language:es
Característica: automatizacion web
  Como paciente
  Quiero realizar la solicitud de una cita medica
  A traves del sistema de Administracion de Hospitales 

  @AgregarDoctor
  Esquema del escenario: Registrar en la pagina
    Dado que Carlos necesita registrar un nuevo doctor
    Cuando el realiza el ingreso del mismo en el aplicativo de Administracion de Hospitales
      | <NombreCompleto> | <Apellidos> | <Telefono> | <TipoDocumento> | <Documento> |
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.

    Ejemplos: 
      | NombreCompleto | Apellidos | Telefono | TipoDocumento      | Documento |
      | John Alexander  | Diaz        | 3008090873  | Cédula de ciudadanía | 778899116   |
      
      
     @RegistrarPaciente
  Esquema del escenario: Registrar en la pagina
    Dado que Carlos necesita registrar un nuevo paciente
    Cuando el realiza el ingreso del mismo en el aplicativo de Administracion de Hospitales agregar paciente
      | <NombreCompleto> | <Apellidos> | <Telefono> | <TipoDocumento> | <Documento> |
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.

    Ejemplos: 
      | NombreCompleto | Apellidos | Telefono | TipoDocumento      | Documento |
      | John Lennon  | Perez       | 3008090873  | Cédula de ciudadanía | 77889934   |

     @AgendarCita
  Esquema del escenario: Registrar en la pagina
    Dado que Carlos necesita asistir al medico
    Cuando el realiza el agendamiento de una Cita
      | <DiaCita> |<DocumentoPaciente> | <DocumentoDoctor> | <Observaciones> |
    Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente.

    Ejemplos: 
      | DiaCita | DocumentoPaciente  | DocumentoDoctor | Observaciones |
      | 09/11/2018  | 77889934       | 778899116   | Me duelen los bolsillos |
      