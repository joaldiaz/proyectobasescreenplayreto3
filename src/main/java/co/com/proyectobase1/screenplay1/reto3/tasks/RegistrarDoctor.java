package co.com.proyectobase1.screenplay1.reto3.tasks;

import java.util.List;


import co.com.proyectobase1.screenplay1.reto3.interactions.Esperar;
import co.com.proyectobase1.screenplay1.reto3.ui.WebAutoDoctorPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class RegistrarDoctor implements Task{
	
	private List<List<String>> data;
	private int i; 
	
	public RegistrarDoctor(List<List<String>> data,int i) {
		super();
		this.data = data;
		this.i=i;
	}
		
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		//actor.attemptsTo(Click.on(WebAutoDoctorPage.CAMPO_NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(0).trim()).into(WebAutoDoctorPage.CAMPO_NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(1).trim()).into(WebAutoDoctorPage.CAMPO_APELLIDO));
		actor.attemptsTo(Enter.theValue(data.get(i).get(2).trim()).into(WebAutoDoctorPage.CAMPO_TELEFONO));
		actor.attemptsTo(Click.on(WebAutoDoctorPage.CAMPO_TIPO_DOC));
		actor.attemptsTo(SelectFromOptions.byVisibleText(data.get(i).get(3)).from(WebAutoDoctorPage.CAMPO_TIPO_DOC));
		actor.attemptsTo(Enter.theValue(data.get(i).get(4).trim()).into(WebAutoDoctorPage.CAMPO_DOCUMENTO));
		actor.attemptsTo(Click.on(WebAutoDoctorPage.BOTON_GUARDAR));
		
	}
	
	public static RegistrarDoctor DoctorEnFormulario(List<List<String>> data, int i) {
		
		return Tasks.instrumented(RegistrarDoctor.class,data,i);
	}

}
