package co.com.proyectobase1.screenplay1.reto3.tasks;


import co.com.proyectobase1.screenplay1.reto3.ui.WebAutoHospitalPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirWebHospital implements Task {

	private WebAutoHospitalPage webAutoHospitalPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(webAutoHospitalPage));
		actor.attemptsTo(Click.on(WebAutoHospitalPage.AGREGAR_DOCTOR));
	}
	
    public static AbrirWebHospital LaPaginaWebHospital() {
		
    	return Tasks.instrumented(AbrirWebHospital.class);
	}

}
