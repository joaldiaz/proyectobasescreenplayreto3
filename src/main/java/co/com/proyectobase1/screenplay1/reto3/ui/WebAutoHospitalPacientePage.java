package co.com.proyectobase1.screenplay1.reto3.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://automatizacion.herokuapp.com/pperez/")
public class WebAutoHospitalPacientePage  extends PageObject{
	
	public static final Target AGREGAR_PACIENTE = Target.the("Opcion para agregar el paciente").located(By.xpath("//A[@href='addPatient']"));
	

}
