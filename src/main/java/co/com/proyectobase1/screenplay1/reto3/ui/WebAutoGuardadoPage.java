package co.com.proyectobase1.screenplay1.reto3.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.serenitybdd.core.annotations.findby.By;

public class WebAutoGuardadoPage extends PageObject {
	public static final Target MENSAJE_EXITOSO = Target.the("EL area donde ya se presenta mensaje exitoso").located(By.xpath("//*[@id='page-wrapper']/div/div[2]/div[2]/p"));

}
