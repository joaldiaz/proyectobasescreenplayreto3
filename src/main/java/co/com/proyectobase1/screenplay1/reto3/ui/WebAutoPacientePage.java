package co.com.proyectobase1.screenplay1.reto3.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class WebAutoPacientePage  extends PageObject{

	public static final Target CAMPO_NOMBRE = Target.the("Campo donde se escribe el nombre").located(By.xpath("(//INPUT[@type='text'])[1]"));
	public static final Target CAMPO_APELLIDO = Target.the("Campo donde se ingresa el apellido").located(By.xpath("(//INPUT[@type='text'])[2]"));
	public static final Target CAMPO_TELEFONO = Target.the("Campo donde se escribe el telefono").located(By.xpath("(//INPUT[@type='text'])[3]"));
	public static final Target CAMPO_TIPO_DOC = Target.the("Campo para ingresar el tipo de documento").located(By.xpath("//SELECT[@class='form-control']"));
	public static final Target CAMPO_DOCUMENTO = Target.the("Campo para ingresar el documento").located(By.xpath("(//INPUT[@type='text'])[4]"));
	public static final Target CAMPO_CHECK = Target.the("Check para seleccionar salud prepagada").located(By.xpath("//INPUT[@type='checkbox']"));
	public static final Target BOTON_GUARDAR = Target.the("Boton para guardar el registro ingresado").located(By.xpath("//A[@onclick='submitForm()'][text()='Guardar']"));
	
	
}
