package co.com.proyectobase1.screenplay1.reto3.tasks;

import java.util.List;

import co.com.proyectobase1.screenplay1.reto3.ui.WebAutoCitaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

public class AgendarPaciente implements Task{
	
	private List<List<String>> data;
	private int i; 
	
	public AgendarPaciente(List<List<String>> data,int i) {
		super();
		this.data = data;
		this.i=i;
	}
		
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		//actor.attemptsTo(Click.on(WebAutoDoctorPage.CAMPO_NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(0).trim()).into(WebAutoCitaPage.CAMPO_DIA_CITA));
		actor.attemptsTo(Enter.theValue(data.get(i).get(1).trim()).into(WebAutoCitaPage.CAMPO_DOC_PACIENTE));
		actor.attemptsTo(Enter.theValue(data.get(i).get(2).trim()).into(WebAutoCitaPage.CAMPO_DOC_DOCTOR));
		actor.attemptsTo(Enter.theValue(data.get(i).get(3).trim()).into(WebAutoCitaPage.CAMPO_OBSERVACIONES));
		actor.attemptsTo(Click.on(WebAutoCitaPage.BOTON_GUARDAR));
		
	}
	
	public static AgendarPaciente PacienteEnCita(List<List<String>> data, int i) {
		
		return Tasks.instrumented(AgendarPaciente.class,data,i);
	}

}