package co.com.proyectobase1.screenplay1.reto3.tasks;

import co.com.proyectobase1.screenplay1.reto3.ui.WebAutoHospitalPacientePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirWebHospitalPaciente implements Task {

	private WebAutoHospitalPacientePage webAutoHospitalPacientePage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(webAutoHospitalPacientePage));
		actor.attemptsTo(Click.on(WebAutoHospitalPacientePage.AGREGAR_PACIENTE));
	}
	
    public static AbrirWebHospitalPaciente LaPaginaWebHospitalPaciente() {
		
    	return Tasks.instrumented(AbrirWebHospitalPaciente.class);
	}

}