package co.com.proyectobase1.screenplay1.reto3.tasks;

import co.com.proyectobase1.screenplay1.reto3.ui.WebAutoHospitalCitaPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirWebHospitalCita implements Task {

	private WebAutoHospitalCitaPage webAutoHospitalCitaPage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Open.browserOn(webAutoHospitalCitaPage));
		actor.attemptsTo(Click.on(WebAutoHospitalCitaPage.AGREGAR_CITA));
	}
	
    public static AbrirWebHospitalCita LaPaginaWebHospitalCita() {
		
    	return Tasks.instrumented(AbrirWebHospitalCita.class);
	}

}
