package co.com.proyectobase1.screenplay1.reto3.interactions;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import java.util.concurrent.TimeUnit;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import org.awaitility.Awaitility;
import static co.com.proyectobase1.screenplay1.reto3.util.UtilidadTiempo.condicionExitosa;

public class Esperar implements Interaction {
	
	private int tiempoEspera;
	
	public Esperar(int tiempoEspera) {
		super();
		this.tiempoEspera = tiempoEspera;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		try {
			Awaitility.await().forever().pollInterval(tiempoEspera, TimeUnit.MILLISECONDS).until(condicionExitosa());
		} catch (Exception e) {
			e.getMessage();
		}
	}
	/**
	 * Hace una pausa de tiempo.
	 * @param tiempoEspera en milisegundos
	 * @return 
	 */
	public static Esperar aMoment(int tiempoEspera) {
		
		Awaitility.await().forever().pollInterval(tiempoEspera, TimeUnit.MILLISECONDS).until(condicionExitosa());
		
		return instrumented(Esperar.class, tiempoEspera);
	}

}
