package co.com.proyectobase1.screenplay1.reto3.questions;

import co.com.proyectobase1.screenplay1.reto3.ui.WebAutoGuardadoPage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaRespuesta implements Question<String>{

	public static LaRespuesta es() {
		
		return new LaRespuesta();
	}

	@Override
	public String answeredBy(Actor actor) {
		
		return Text.of(WebAutoGuardadoPage.MENSAJE_EXITOSO).viewedBy(actor).asString();
	}

}
