package co.com.proyectobase1.screenplay1.reto3.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class WebAutoDoctorPage  extends PageObject{
	
	
	public static final Target CAMPO_NOMBRE = Target.the("Campo donde se escribe el nombre").located(By.id("name"));
	public static final Target CAMPO_APELLIDO = Target.the("Campo donde se ingresa el apellido").located(By.id("last_name"));
	public static final Target CAMPO_TELEFONO = Target.the("Campo donde se escribe el telefono").located(By.id("telephone"));
	public static final Target CAMPO_TIPO_DOC = Target.the("Campo para ingresar el tipo de documento").located(By.id("identification_type"));
	public static final Target CAMPO_DOCUMENTO = Target.the("Campo para ingresar el documento").located(By.id("identification"));
	public static final Target BOTON_GUARDAR = Target.the("Boton para guardar el registro ingresado").located(By.xpath("//A[@onclick='submitForm()'][text()='Guardar']"));
	

}
