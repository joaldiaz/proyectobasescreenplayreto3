package co.com.proyectobase1.screenplay1.reto3.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class WebAutoCitaPage  extends PageObject{

	public static final Target CAMPO_DIA_CITA = Target.the("Campo donde se agenda el dia de al cita").located(By.id("datepicker"));
	public static final Target CAMPO_DOC_PACIENTE = Target.the("Campo para ingresar el documeno del paciente").located(By.xpath("(//INPUT[@type='text'])[2]"));
	public static final Target CAMPO_DOC_DOCTOR = Target.the("Campo para ingresar el documento del doctor").located(By.xpath("(//INPUT[@type='text'])[3]"));
	public static final Target CAMPO_OBSERVACIONES = Target.the("campo para ingresar las observaciones").located(By.xpath("//TEXTAREA[@class='form-control']"));
	public static final Target BOTON_GUARDAR = Target.the("Boton para guardar el registro ingresado").located(By.xpath("//A[@onclick='submitForm()'][text()='Guardar']"));
	
	
}
